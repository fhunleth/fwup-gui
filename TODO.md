# TODO

1. Support >1 public key - ideally, this is a feature for `fwup` so that the .fw file doesn't need to be run through fwup n times
2. Support unsigned .fw files when public keys are registered. This would also be nice for fwup to support via commandline args. I think that the semantics are that everything is ok and the verification message says what happened.
3. Figure out Windows privilege elevation and fix fwup-gui to do it so that scaning destinations works.


## fwup fixes

1. Change fwup's verification message to be more user friendly and be put in the "success" field rather than just be a warning.
2. Support >1 public key
