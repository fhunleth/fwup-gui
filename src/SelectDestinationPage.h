#ifndef SELECTDESTINATIONPAGE_H
#define SELECTDESTINATIONPAGE_H

#include <QWizardPage>
#include "Fwup.h"

namespace Ui {
class SelectDestinationPage;
}

class QTimer;

class SelectDestinationPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit SelectDestinationPage(QWidget *parent = 0);
    ~SelectDestinationPage();

protected:
    void initializePage();
    bool isComplete() const;
    bool validatePage();

private slots:
    void onFwupSuccess(const QString &result);
    void onFwupFail(const QString &message);
    void onDrivePoller();

private:
    QString toPrettySize(quint64 size) const;

private:
    Ui::SelectDestinationPage *ui;
    Fwup *fwup_;
    QTimer *drivePoller_;
    QList<Fwup::Destination> currentDestinations_;
};

#endif // SELECTDESTINATIONPAGE_H
