#include <QApplication>
#include "FwupWizard.h"

int main(int argc, char *argv[])
{
    QApplication::setOrganizationName("Troodon Software, LLC");
    QApplication::setOrganizationDomain("troodon-software.com");
    QApplication::setApplicationName("The Fwup Pup");
    QApplication::setApplicationDisplayName(QObject::tr("The Fwup Pup"));
    QApplication::setApplicationVersion("0.1.0");

    QApplication a(argc, argv);
    FwupWizard w;
    w.show();

    return a.exec();
}
