#include "NamedKeyTableModel.h"

NamedKeyTableModel::NamedKeyTableModel(QList<NamedKey> *keys, QObject *parent) :
    QAbstractTableModel(parent),
    keys_(keys)
{

}

int NamedKeyTableModel::rowCount(const QModelIndex &) const
{
    return keys_->count();
}

bool NamedKeyTableModel::removeRows(int row, int count, const QModelIndex &parent)
{
    if (count != 1)
        qFatal("count is supposed to be 1");

    beginRemoveRows(parent, row, row);
    keys_->removeAt(row);
    endRemoveRows();
    return true;
}

bool NamedKeyTableModel::append(const NamedKey &namedKey)
{
    beginInsertRows(QModelIndex(), keys_->count(), keys_->count());
    keys_->append(namedKey);
    endInsertRows();
    return true;
}

int NamedKeyTableModel::columnCount(const QModelIndex &) const
{
    return 2;
}

QVariant NamedKeyTableModel::data(const QModelIndex &index, int role) const
{
    if (role != Qt::DisplayRole)
        return QVariant();

    int ix = index.row();
    if (ix < 0 || ix >= keys_->count())
        return QVariant();

    switch (index.column()) {
    case 0:
        return keys_->at(ix).name;
    case 1:
        return keys_->at(ix).key;
    default:
        return QVariant();
    }
}

QVariant NamedKeyTableModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if (role != Qt::DisplayRole || orientation != Qt::Horizontal)
        return QVariant();

    switch (section) {
    case 0:
        return tr("Name");

    case 1:
        return tr("Key");

    default:
        return QVariant();
    }
}
