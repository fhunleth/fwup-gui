#ifndef NAMEDKEY_H
#define NAMEDKEY_H

#include <QString>

struct NamedKey {
    NamedKey() {}
    NamedKey(const QString &n, const QString &k) : name(n), key(k) {}

    QString name;
    QString key;
};

QList<NamedKey> readPublicKeys(bool *skipSignatureCheck);
void writePublicKeys(const QList<NamedKey> &keys, bool skipSignatureCheck);

#endif // NAMEDKEY_H
