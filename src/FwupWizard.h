#ifndef FWUPWIZARD_H
#define FWUPWIZARD_H

#include <QWizard>

class FwupWizard : public QWizard
{
    Q_OBJECT
public:
    FwupWizard();

private:
    enum { Page_SelectFile, Page_ConfirmFile, Page_SelectDestination, Page_Burn };
};

#endif // FWUPWIZARD_H
