#include "SelectFilePage.h"
#include "ui_SelectFilePage.h"
#include "Fwup.h"
#include <QFile>
#include <QFileDialog>
#include <QSettings>
#include "ManageKeysDialog.h"

SelectFilePage::SelectFilePage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::SelectFilePage),
    isValid_(false)
{
    ui->setupUi(this);

    fwup_ = new Fwup(this);
    connect(fwup_, SIGNAL(success(QString)), SLOT(onFwupSuccess(QString)));
    connect(fwup_, SIGNAL(error(QString)), SLOT(onFwupFail(QString)));

    registerField("fwfile", ui->firmwareFileLineEdit);
}

SelectFilePage::~SelectFilePage()
{
    delete ui;
}

void SelectFilePage::on_firmwareFileLineEdit_textChanged(const QString &input)
{
    validateFw(input);
}

void SelectFilePage::validateFw(const QString &path)
{
    if (!path.endsWith(".fw")) {
        setSelectionStatus(false, "");
        return;
    }

    if (!QFile::exists(path)) {
        setSelectionStatus(false, tr("File not found"));
        return;
    }

    fwup_->validate(path);
}

void SelectFilePage::onFwupSuccess(const QString &result)
{
    setSelectionStatus(true, result);
}

void SelectFilePage::onFwupFail(const QString &message)
{
    setSelectionStatus(false, tr("Validation failed: %1").arg(message));
}

void SelectFilePage::on_fileBrowseButton_clicked()
{
    QSettings settings;
    QString dir = settings.value("FirmwareDirectory").toString();
    QFileDialog dialog(this,
                       tr("Select firmware"),
                       settings.value("FirmwareDirectory").toString(),
                       tr("Firmware Files (*.fw)"));
    if (dialog.exec()) {
        QString filename = dialog.selectedFiles().at(0);
        ui->firmwareFileLineEdit->setText(filename);
        settings.setValue("FirmwareDirectory", dialog.directory().canonicalPath());
    }
}

void SelectFilePage::on_manageKeysButton_clicked()
{
    bool skipSignatureCheck;
    QList<NamedKey> keys = readPublicKeys(&skipSignatureCheck);
    ManageKeysDialog dialog(keys, skipSignatureCheck, this);
    if (dialog.exec()) {
        keys = dialog.keys();
        writePublicKeys(keys, skipSignatureCheck);
        validateFw(ui->firmwareFileLineEdit->text());
    }
}

void SelectFilePage::setSelectionStatus(bool isValid, const QString &status)
{
    ui->statusLabel->setText(status);

    if (isValid_ != isValid) {
        isValid_ = isValid;
        updateStatusStyle();
        emit completeChanged();
    }
}

void SelectFilePage::updateStatusStyle()
{
    if (!isValid_)
        ui->statusLabel->setStyleSheet("color: red");
    else
        ui->statusLabel->setStyleSheet(QString());
}

void SelectFilePage::initializePage()
{
    updateStatusStyle();

    QSettings settings;
    ui->firmwareFileLineEdit->setText(settings.value("LastFirmwarePathname").toString());
}

bool SelectFilePage::isComplete() const
{
    return isValid_;
}

bool SelectFilePage::validatePage()
{
    QSettings settings;
    settings.setValue("LastFirmwarePathname", ui->firmwareFileLineEdit->text());
    return true;
}
