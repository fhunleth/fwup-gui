#include "ConfirmFilePage.h"
#include "ui_ConfirmFilePage.h"

#include "Fwup.h"
#include <QMessageBox>
#include <QLabel>

static void addIfContains(QStringList *keys, const QString &what, QStringList *result)
{
    if (keys->contains(what)) {
        keys->removeAll(what);
        result->append(what);
    }
}

static QStringList sortMetadata(const QStringList &keys)
{
    QStringList copy = keys;
    QStringList sorted;
    addIfContains(&copy, "meta-product", &sorted);
    addIfContains(&copy, "meta-description", &sorted);
    addIfContains(&copy, "meta-version", &sorted);
    addIfContains(&copy, "meta-vcs-identifier", &sorted);
    addIfContains(&copy, "meta-creation-date", &sorted);
    addIfContains(&copy, "meta-author", &sorted);
    addIfContains(&copy, "meta-platform", &sorted);
    addIfContains(&copy, "meta-architecture", &sorted);
    addIfContains(&copy, "meta-misc", &sorted);
    qSort(copy);
    sorted << copy;
    return sorted;
}

ConfirmFilePage::ConfirmFilePage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::ConfirmFilePage)
{
    ui->setupUi(this);
    fwup_ = new Fwup(this);
    connect(fwup_, SIGNAL(success(QString)), SLOT(onFwupSuccess(QString)));
    connect(fwup_, SIGNAL(error(QString)), SLOT(onFwupFail(QString)));
}

ConfirmFilePage::~ConfirmFilePage()
{
    delete ui;
}

void ConfirmFilePage::initializePage()
{
    metadataGood_ = false;
    updateMetadata();
}

bool ConfirmFilePage::validatePage()
{
    return metadataGood_;
}

bool ConfirmFilePage::isComplete() const
{
    return metadataGood_;
}

void ConfirmFilePage::onFwupSuccess(const QString &result)
{
    QMap<QString, QString> metadata = fwup_->parseMetadataResult(result);
    if (metadata.count() == 0) {
        ui->metadataFormLayout->addWidget(new QLabel(tr("Error: No metadata found")));
        metadataGood_ = true;
    } else {
        QStringList keys = sortMetadata(metadata.keys());
        for (QStringList::const_iterator i = keys.constBegin();
             i != keys.constEnd();
             ++i) {
            QString keyText = metadataKeyToText(*i);
            if (!keyText.isEmpty()) {
                QWidget *label = new QLabel(metadataKeyToText(*i), ui->metadataGroupBox);
                QWidget *field = new QLabel(metadata.value(*i), ui->metadataGroupBox);
                ui->metadataFormLayout->addRow(label, field);
            }
        }
        metadataGood_ = true;
    }
    emit completeChanged();
}

void ConfirmFilePage::onFwupFail(const QString &message)
{
    ui->metadataFormLayout->addWidget(new QLabel(tr("Error: %1").arg(message)));
    metadataGood_ = false;
    emit completeChanged();
}

QString ConfirmFilePage::metadataKeyToText(const QString &key) const
{
    if (key == "meta-architecture")
        return tr("Architecture");
    else if (key == "meta-product")
        return tr("Product");
    else if (key == "meta-description")
        return tr("Description");
    else if (key == "meta-version")
        return tr("Version");
    else if (key == "meta-author")
        return tr("Author");
    else if (key == "meta-platform")
        return tr("Platform");
    else if (key == "meta-vcs-identifier")
        return tr("VCS Identifier");
    else if (key == "meta-misc")
        return tr("Misc");
    else if (key == "meta-creation-date")
        return tr("Creation date");
    else if (key == "meta-fwup-version")
        return QString();
    else
        return tr("Custom (%1)").arg(key);
}

void ConfirmFilePage::updateMetadata()
{
    foreach (QObject* child, ui->metadataGroupBox->children())
        child->deleteLater();
    delete ui->metadataFormLayout;

    ui->metadataFormLayout = new QFormLayout(ui->metadataGroupBox);
    ui->metadataFormLayout->setSpacing(6);
    ui->metadataFormLayout->setContentsMargins(11, 11, 11, 11);

    fwup_->metadata(field("fwfile").toString());
}
