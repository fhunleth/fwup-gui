#include "Fwup.h"
#include "NamedKey.h"

#include <QProcess>
#include <QApplication>
#include <QStandardPaths>

Fwup::Fwup(QObject *parent) :
    QObject(parent),
    process_(0)
{

}

static void addPublicKeyArguments(QStringList *args)
{
    bool skipSignatureCheck;
    QList<NamedKey> keys = readPublicKeys(&skipSignatureCheck);

    if (skipSignatureCheck)
        return;

    foreach (NamedKey key, keys)
        *args << "--public-key" << key.key;
}

void Fwup::burn(const QString &firmwareFilename, const QString &destination, const QString &task)
{
    QStringList args;
    args << "--apply"
         << "-i"
         << firmwareFilename
         << "-d"
         << destination
         << "--task"
         << task;

    addPublicKeyArguments(&args);

    start(args, true);
}

void Fwup::metadata(const QString &firmwareFilename)
{
    QStringList args;
    args << "--metadata"
         << "-i"
         << firmwareFilename;

    addPublicKeyArguments(&args);
    start(args);
}

void Fwup::tasks(const QString &firmwareFilename)
{
    QStringList args;
    args << "--list"
         << "-i"
         << firmwareFilename;

    addPublicKeyArguments(&args);
    start(args);
}

void Fwup::destinations()
{
    start(QStringList() << "--detect");
}

void Fwup::validate(const QString &firmwareFilename)
{
    QStringList args;
    args << "--verify"
         << "-i"
         << firmwareFilename;

    addPublicKeyArguments(&args);
    start(args);
}

QStringList Fwup::parseTasksResult(const QString &result)
{
    return result.split("\n");
}

QList<Fwup::Destination> Fwup::parseDestinationsResult(const QString &result)
{
    QList<Fwup::Destination> destinations;

    QStringList lines = result.split("\n");
    foreach (QString line, lines) {
        QStringList parts = line.split(",");
        if (line.count() > 0) {
            struct Destination d;
            d.path = parts.at(0);
            d.size = parts.count() > 0 ? parts.at(1).toLongLong() : 0;
            destinations.append(d);
        }
    }

    return destinations;
}

QMap<QString, QString> Fwup::parseMetadataResult(const QString &result)
{
    QMap<QString, QString> metadata;

    QStringList lines = result.split("\n");
    foreach (QString line, lines) {
        QStringList parts = line.split("=");
        if (line.count() > 0 && line.at(0) != '#' && parts.count() == 2) {
            QString value = parts.at(1);
            if (value.at(0) == '"')
                value = value.mid(1, value.count() - 2);
            metadata.insert(parts.at(0), value);
        }
    }

    return metadata;
}

static QString fwupPath()
{
#if defined(__APPLE__)
    static QString path = "./fwup";
#elif defined(__WIN32)
    static QString path = "fwup.exe";
#else
    static QString path = QStandardPaths::findExecutable("fwup");
#endif
    return path;
}

void Fwup::start(const QStringList &args, bool sudoRequired)
{
    if (process_)
        qFatal("Already started?");

    QString program = fwupPath();

    QStringList allArgs;
    if (sudoRequired) {
#if __linux__
        program = QStandardPaths::findExecutable("gksudo");
        if (program.isEmpty()) {
            emit error(tr("gksudo not found. Please install and retry."));
            return;
        }
        allArgs << "--description"
                << QApplication::applicationDisplayName()
                << "--"
                << fwupPath();
#endif
    }
    allArgs.append("--framing");
    allArgs.append(args);

    reportedCompletion_ = false;
    buffer_.clear();
    process_ = new QProcess(this);
    connect(process_, SIGNAL(readyRead()), SLOT(onReadyRead()));
    connect(process_, SIGNAL(finished(int,QProcess::ExitStatus)), SLOT(onFinished(int,QProcess::ExitStatus)));
    connect(process_, SIGNAL(errorOccurred(QProcess::ProcessError)), SLOT(onError(QProcess::ProcessError)));

    process_->start(program, allArgs, QIODevice::ReadOnly);
}

void Fwup::handleFwupMessage(const QByteArray &msg)
{
    if (msg.at(0) == 'O' && msg.at(1) == 'K') {
        QString result = QString::fromUtf8(msg.mid(4));
        qDebug("fwup returned success: %s", qPrintable(result));
        reportedCompletion_ = true;
        emit success(result);
    } else if (msg.at(0) == 'E' && msg.at(1) == 'R') {
        QString reason = QString::fromUtf8(msg.mid(4));
        qDebug("fwup returned error: %s", qPrintable(reason));
        reportedCompletion_ = true;
        emit error(reason);
    } else if (msg.at(0) == 'P' && msg.at(1) == 'R') {
        int value = ((int) msg.at(2) << 8) | msg.at(3);
        emit progress(value);
    } else if (msg.at(0) == 'W' && msg.at(1) == 'N') {
        QString reason = QString::fromUtf8(msg.mid(4));
        qWarning("fwup returned a warning: %s", qPrintable(reason));
    } else {
        qWarning("fwup outputing unexpected text: %s", msg.constData());

        emit error(tr("Unexpected output: %s", msg.constData()));
        reportedCompletion_ = true;
        process_->kill();
    }
}

void Fwup::onReadyRead()
{
    buffer_.append(process_->readAll());
    for (;;) {
        if (buffer_.count() <= 4)
            return;

        int size = (((uint8_t) buffer_.at(0)) << 24) |
                (((uint8_t) buffer_.at(1)) << 16) |
                (((uint8_t) buffer_.at(2)) << 8) |
                ((uint8_t) buffer_.at(3));
        if (buffer_.count() < 4 + size)
            return;

        QByteArray msg = buffer_.mid(4, size);
        buffer_ = buffer_.mid(4 + size);

        handleFwupMessage(msg);
    }
}

void Fwup::cleanupProcess()
{
    if (!process_)
        return;

    process_->deleteLater();
    process_ = 0;
    buffer_.clear();
}

void Fwup::onFinished(int rc, QProcess::ExitStatus status)
{
    cleanupProcess();

    if (reportedCompletion_)
        return;
    reportedCompletion_ = true;

    if (status == QProcess::CrashExit)
        emit error(tr("fwup crashed"));
    else if (rc == 0)
        emit success(QString());
    else
        emit error(tr("fwup exited unexpectantly with code %1").arg(rc));
}

void Fwup::onError(QProcess::ProcessError err)
{
    cleanupProcess();

    if (reportedCompletion_)
        return;
    reportedCompletion_ = true;

    switch (err) {
    case QProcess::FailedToStart:
        emit error(tr("Failed to find fwup. Try reinstalling"));
        break;

    case QProcess::Crashed:
        emit error(tr("fwup crashed"));
        break;

    case QProcess::Timedout:
        emit error(tr("fwup timed out"));
        break;

    case QProcess::ReadError:
        emit error(tr("Error reading from fwup"));
        break;

    case QProcess::WriteError:
        // Huh?
        emit error(tr("Error communicating with fwup"));
        break;

    case QProcess::UnknownError:
    default:
        emit error(tr("Unknown error running fwup."));
        break;
    }
}
