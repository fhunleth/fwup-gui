#include "NamedKey.h"
#include <QSettings>

QList<NamedKey> readPublicKeys(bool *skipSignatureCheck)
{
    QSettings settings;
    QList<NamedKey> keys;
    int numKeys = settings.beginReadArray("keys");
    for (int i = 0; i < numKeys; i++) {
        settings.setArrayIndex(i);
        NamedKey key;
        key.name = settings.value("name").toString();
        key.key = settings.value("key").toString();
        keys.append(key);
    }
    settings.endArray();
    *skipSignatureCheck = settings.value("skipSignatureCheck", false).toBool();

    return keys;
}

void writePublicKeys(const QList<NamedKey> &keys, bool skipSignatureCheck)
{
    QSettings settings;
    settings.beginWriteArray("keys");
    for (int i = 0; i < keys.count(); i++) {
        settings.setArrayIndex(i);
        settings.setValue("name", keys.at(i).name);
        settings.setValue("key", keys.at(i).key);
    }
    settings.endArray();
    settings.setValue("skipSignatureCheck", skipSignatureCheck);
}
