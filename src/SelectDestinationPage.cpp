#include "SelectDestinationPage.h"
#include "ui_SelectDestinationPage.h"

#include <QTimer>

SelectDestinationPage::SelectDestinationPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::SelectDestinationPage)
{
    ui->setupUi(this);
    fwup_ = new Fwup(this);
    connect(fwup_, SIGNAL(error(QString)), SLOT(onFwupFail(QString)));
    connect(fwup_, SIGNAL(success(QString)), SLOT(onFwupSuccess(QString)));

    drivePoller_ = new QTimer(this);
    drivePoller_->setInterval(1000);
    connect(drivePoller_, SIGNAL(timeout()), SLOT(onDrivePoller()));

    registerField("dest", ui->destinationComboBox, "currentText", "currentTextChanged");

    // Once the user clicks next, they can't go back.
    setCommitPage(true);
    setButtonText(QWizard::CommitButton, tr("Burn"));
}

SelectDestinationPage::~SelectDestinationPage()
{
    delete ui;
}

QString SelectDestinationPage::toPrettySize(quint64 size) const
{
    if (size > 1000000000) {
        double gigs = size / 1000000000;
        return tr("%1 GB").arg(gigs, 0, 'f', 1);
    } else if (size > 1000000) {
        double megs = size / 1000000;
        return tr("%1 MB").arg(megs, 0, 'f', 1);
    } else if (size > 1000) {
        double kbs = size / 1000;
        return tr("%1 KB").arg(kbs, 0, 'f', 1);
    } else
        return tr("%1 bytes").arg(size);
}

void SelectDestinationPage::onDrivePoller()
{
    fwup_->destinations();
}

void SelectDestinationPage::initializePage()
{
    drivePoller_->start();
}

bool SelectDestinationPage::isComplete() const
{
    return ui->destinationComboBox->currentData().isValid();
}

bool SelectDestinationPage::validatePage()
{
    drivePoller_->stop();
    return true;
}

void SelectDestinationPage::onFwupSuccess(const QString &result)
{
    QList<Fwup::Destination> dest = fwup_->parseDestinationsResult(result);
    if (dest == currentDestinations_)
        return;

    QString oldSelection = ui->destinationComboBox->currentData().toString();

    ui->destinationComboBox->clear();
    for (int i = 0; i < dest.count(); i++) {
        QString label;
        if (dest.at(i).size == 0)
            label = dest.at(i).path;
        else
            label = tr("%1 [%2]").arg(dest.at(i).path)
                        .arg(toPrettySize(dest.at(i).size));
        ui->destinationComboBox->addItem(label, dest.at(i).path);
        if (dest.at(i).path == oldSelection)
            ui->destinationComboBox->setCurrentIndex(i);
    }
    currentDestinations_ = dest;
    emit completeChanged();

}

void SelectDestinationPage::onFwupFail(const QString &message)
{
    // Not sure what to do here.
    qCritical("fwup failed? %s", qPrintable(message));
}
