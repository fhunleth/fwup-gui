#ifndef NAMEDKEYTABLEMODEL_H
#define NAMEDKEYTABLEMODEL_H

#include <QAbstractTableModel>
#include "NamedKey.h"

class NamedKeyTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit NamedKeyTableModel(QList<NamedKey> *keys, QObject *parent = nullptr);

    int rowCount(const QModelIndex &parent) const;
    int columnCount(const QModelIndex &parent) const;
    QVariant data(const QModelIndex &index, int role) const;
    QVariant headerData(int section, Qt::Orientation orientation, int role) const;

    bool append(const NamedKey &namedKey);

protected:
    bool removeRows(int row, int count, const QModelIndex &parent);

private:

    QList<NamedKey> *keys_;
};

#endif // NAMEDKEYTABLEMODEL_H
