#include "AddKeyDialog.h"
#include "ui_AddKeyDialog.h"

AddKeyDialog::AddKeyDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::AddKeyDialog)
{
    ui->setupUi(this);
    setWindowTitle(QApplication::applicationDisplayName());
}

AddKeyDialog::~AddKeyDialog()
{
    delete ui;
}

NamedKey AddKeyDialog::namedKey() const
{
    return NamedKey(ui->nameLineEdit->text(), ui->keyLineEdit->text());
}
