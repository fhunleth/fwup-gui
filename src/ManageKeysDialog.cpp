#include "ManageKeysDialog.h"
#include "ui_ManageKeysDialog.h"
#include "NamedKeyTableModel.h"
#include "AddKeyDialog.h"

#include <QMessageBox>

ManageKeysDialog::ManageKeysDialog(const QList<NamedKey> &keys, bool skipSignatureCheck, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ManageKeysDialog),
    keys_(keys)
{
    ui->setupUi(this);
    setWindowTitle(QApplication::applicationDisplayName());

    model_ = new NamedKeyTableModel(&keys_, this);
    ui->keyTableView->setModel(model_);
    ui->allowUnsignedCheckBox->setChecked(skipSignatureCheck);

    connect(ui->keyTableView->selectionModel(), SIGNAL(selectionChanged(QItemSelection,QItemSelection)), SLOT(onSelectionChanged(QItemSelection,QItemSelection)));
}

ManageKeysDialog::~ManageKeysDialog()
{
    delete ui;
}

QList<NamedKey> ManageKeysDialog::keys() const
{
    return keys_;
}

bool ManageKeysDialog::skipSignatureCheck() const
{
    return ui->allowUnsignedCheckBox->isChecked();
}

void ManageKeysDialog::on_addButton_clicked()
{
    AddKeyDialog dialog(this);
    if (dialog.exec()) {
        NamedKey nk = dialog.namedKey();
        if (nk.name.isEmpty())
            nk.name = tr("unnamed");
        QByteArray decodedKey = QByteArray::fromBase64(nk.key.toUtf8());
        if (decodedKey.count() != 32) {
            QMessageBox::critical(this, windowTitle(), tr("Key has invalid characters or isn't the right length."));
            return;
        }
        nk.key = decodedKey.toBase64();
        model_->append(nk);
    }
}

void ManageKeysDialog::on_removeButton_clicked()
{
    QModelIndex ix = ui->keyTableView->currentIndex();
    model_->removeRow(ix.row());
}

void ManageKeysDialog::onSelectionChanged(const QItemSelection &current, const QItemSelection &)
{
    ui->removeButton->setEnabled(!current.isEmpty());
}
