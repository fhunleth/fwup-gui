#ifndef MANAGEKEYSDIALOG_H
#define MANAGEKEYSDIALOG_H

#include <QDialog>
#include "NamedKey.h"

namespace Ui {
class ManageKeysDialog;
}

class NamedKeyTableModel;
class QItemSelection;

class ManageKeysDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ManageKeysDialog(const QList<NamedKey> &keys, bool skipSignatureCheck, QWidget *parent = 0);
    ~ManageKeysDialog();

    QList<NamedKey> keys() const;
    bool skipSignatureCheck() const;

private slots:
    void on_addButton_clicked();
    void on_removeButton_clicked();

    void onSelectionChanged(const QItemSelection &current, const QItemSelection &previous);
private:
    Ui::ManageKeysDialog *ui;
    QList<NamedKey> keys_;
    NamedKeyTableModel *model_;
};

#endif // MANAGEKEYSDIALOG_H
