#ifndef ADDKEYDIALOG_H
#define ADDKEYDIALOG_H

#include <QDialog>
#include "NamedKey.h"

namespace Ui {
class AddKeyDialog;
}

class AddKeyDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddKeyDialog(QWidget *parent = 0);
    ~AddKeyDialog();

    NamedKey namedKey() const;

private:
    Ui::AddKeyDialog *ui;
};

#endif // ADDKEYDIALOG_H
