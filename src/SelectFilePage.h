#ifndef SELECTFILEPAGE_H
#define SELECTFILEPAGE_H

#include <QWizardPage>

namespace Ui {
class SelectFilePage;
}

class Fwup;

class SelectFilePage : public QWizardPage
{
    Q_OBJECT

public:
    explicit SelectFilePage(QWidget *parent = 0);
    ~SelectFilePage();

protected:
    void initializePage();
    bool isComplete() const;
    bool validatePage();

private slots:
    void on_firmwareFileLineEdit_textChanged(const QString &text);
    void on_manageKeysButton_clicked();
    void on_fileBrowseButton_clicked();

    void onFwupSuccess(const QString &result);
    void onFwupFail(const QString &message);

private:
    void setSelectionStatus(bool isValid, const QString &status);
    void updateStatusStyle();
    void validateFw(const QString &path);

private:
    Ui::SelectFilePage *ui;
    bool isValid_;
    Fwup *fwup_;
};

#endif // SELECTFILEPAGE_H
