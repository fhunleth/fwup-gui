#include "FwupWizard.h"

#include "BurningPage.h"
#include "ConfirmFilePage.h"
#include "SelectDestinationPage.h"
#include "SelectFilePage.h"

#include <QIcon>

FwupWizard::FwupWizard() :
    QWizard()
{
    QPixmap fwupPup(":/ui/fwup-pup64x64.png");

    setWindowIcon(QIcon(fwupPup));
    setPage(Page_SelectFile, new SelectFilePage);
    setPage(Page_ConfirmFile, new ConfirmFilePage);
    setPage(Page_SelectDestination, new SelectDestinationPage);
    setPage(Page_Burn, new BurningPage);

    setPixmap(QWizard::LogoPixmap, fwupPup);
    setPixmap(QWizard::BackgroundPixmap, QPixmap(":/ui/part-fwup-pup.png"));

    // Not sure about the following ones:
    //setPixmap(QWizard::BannerPixmap, QPixmap(":/ui/fwup-pup256x256.png"));
    //setPixmap(QWizard::WatermarkPixmap, QPixmap(":/ui/fwup-pup256x256.png"));
}
