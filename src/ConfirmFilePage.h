#ifndef CONFIRMFILEPAGE_H
#define CONFIRMFILEPAGE_H

#include <QWizardPage>

namespace Ui {
class ConfirmFilePage;
}

class Fwup;

class ConfirmFilePage : public QWizardPage
{
    Q_OBJECT

public:
    explicit ConfirmFilePage(QWidget *parent = 0);
    ~ConfirmFilePage();

protected:
    void initializePage();
    bool validatePage();
    bool isComplete() const;

private slots:
    void onFwupSuccess(const QString &result);
    void onFwupFail(const QString &message);

private:
    QString metadataKeyToText(const QString &key) const;
    void updateMetadata();

private:
    Ui::ConfirmFilePage *ui;
    bool metadataGood_;
    Fwup *fwup_;
};

#endif // CONFIRMFILEPAGE_H
