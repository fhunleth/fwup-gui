#ifndef BURNINGPAGE_H
#define BURNINGPAGE_H

#include <QWizardPage>

namespace Ui {
class BurningPage;
}

class Fwup;

class BurningPage : public QWizardPage
{
    Q_OBJECT

public:
    explicit BurningPage(QWidget *parent = 0);
    ~BurningPage();

protected:
    void initializePage();    
    bool validatePage();
    bool isComplete() const;

private slots:
    void onSuccess(const QString &result);
    void onError(const QString &reason);
    void onProgress(int value);

private:
    Ui::BurningPage *ui;
    Fwup *fwup_;
    bool isComplete_;
};

#endif // BURNINGPAGE_H
