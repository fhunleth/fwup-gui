#include "BurningPage.h"
#include "ui_BurningPage.h"
#include "Fwup.h"

#include <QMessageBox>
#include <QFileInfo>

BurningPage::BurningPage(QWidget *parent) :
    QWizardPage(parent),
    ui(new Ui::BurningPage),
    isComplete_(false)
{
    ui->setupUi(this);
    fwup_ = new Fwup(this);
    connect(fwup_, SIGNAL(progress(int)), SLOT(onProgress(int)));
    connect(fwup_, SIGNAL(error(QString)), SLOT(onError(QString)));
    connect(fwup_, SIGNAL(success(QString)), SLOT(onSuccess(QString)));

    setButtonText(QWizard::CustomButton1, tr("Program another"));
}

BurningPage::~BurningPage()
{
    delete ui;
}

void BurningPage::initializePage()
{
    isComplete_ = false;
    ui->progressBar->setValue(0);

    QFileInfo fi(field("fwfile").toString());
    QString destWithInfo = field("dest").toString();
    QStringList parts = destWithInfo.split(QLatin1Char(' '));
    QString dest = parts.at(0);

    ui->burningFirmwareLabel->setText(tr("Burning %1 to %2...").arg(fi.fileName()).arg(dest));
    ui->statusLabel->setText(tr("In progress"));

    fwup_->burn(fi.filePath(), dest, "complete");
}

bool BurningPage::isComplete() const
{
    return isComplete_;
}

bool BurningPage::validatePage()
{
    return true;
}

void BurningPage::onProgress(int value)
{
    ui->progressBar->setValue(value);
}

void BurningPage::onSuccess(const QString &)
{
    ui->statusLabel->setText(tr("Success!"));
    isComplete_ = true;
    emit completeChanged();
}

void BurningPage::onError(const QString &reason)
{
    QMessageBox::critical(this, "fwup", reason);
    ui->statusLabel->setText(tr("Failed: %1").arg(reason));
    isComplete_ = true;
    emit completeChanged();
}
