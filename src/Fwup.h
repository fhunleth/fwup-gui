#ifndef FWUP_H
#define FWUP_H

#include <QProcess>

class Fwup : public QObject
{
    Q_OBJECT
public:
    struct Destination {
        QString path;
        quint64 size;

        bool operator==(const Destination &o) const { return o.path == path && o.size == size; }
    };

    explicit Fwup(QObject *parent = nullptr);

    void burn(const QString &firmwareFilename, const QString &destination, const QString &task);
    void metadata(const QString &firmwareFilename);
    void tasks(const QString &firmwareFilename);
    void destinations();
    void validate(const QString &firmwareFilename);

    static QStringList parseTasksResult(const QString &result);
    static QList<Destination> parseDestinationsResult(const QString &result);
    static QMap<QString,QString> parseMetadataResult(const QString &result);

    void start(const QStringList &args, bool sudoRequired = false);

signals:
    void progress(int);
    void error(QString reason);
    void success(QString result);

private slots:
    void onReadyRead();
    void onFinished(int rc, QProcess::ExitStatus status);
    void onError(QProcess::ProcessError error);

private:
    void handleFwupMessage(const QByteArray &msg);
    void cleanupProcess();

private:
    QProcess *process_;
    QByteArray buffer_;
    bool reportedCompletion_;
};

#endif // FWUP_H
