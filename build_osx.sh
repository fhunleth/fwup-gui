#!/bin/sh

set -e

VERSION=$(git describe --always)
APP=fwup-gui
[ -z $QMAKE ] && QMAKE=qmake
[ -z $MACDEPLOYQT ] && MACDEPLOYQT=$(dirname $(which $QMAKE))/macdeployqt

rm -fr build
mkdir build
cd build
$QMAKE ../$APP.pro
make

$MACDEPLOYQT $APP.app
cp ../work/fwup-1.0.0/staging/fwup fwup-gui.app/Contents/MacOS/fwup

#mv -f $APP.dmg $APP-$VERSION.dmg
