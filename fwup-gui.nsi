; Pass in the VERSION via the commandline: /DVERSION=x.y.z.w

!define BUILD_PRODUCTS_BASE "./staging"
!include MUI2.nsh
!include Library.nsh

Name "fwup-gui ${VERSION}"

; The file to write
OutFile "fwup-gui-${VERSION}.exe"
SetCompressor lzma

; Branding
;;;!define MUI_ICON eye.ico
BrandingText "Frank Hunleth"

; The default installation directory
InstallDir $PROGRAMFILES\FrankHunleth\fwup-gui

; Registry key to check for directory (so if you install again, it will
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\FrankHunleth\fwup-gui" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages

;!define MUI_HEADERIMAGE
!define MUI_HEADERIMAGE_BITMAP "${NSISDIR}\Contrib\Graphics\Header\nsis.bmp" ; optional
!define MUI_ABORTWARNING

;Page components
!insertmacro MUI_PAGE_WELCOME
!insertmacro MUI_PAGE_DIRECTORY
!insertmacro MUI_PAGE_INSTFILES
!insertmacro MUI_PAGE_FINISH

!insertmacro MUI_UNPAGE_CONFIRM
!insertmacro MUI_UNPAGE_INSTFILES

;--------------------------------
; Languages
!insertmacro MUI_LANGUAGE "English"

; The stuff to install
Section "Main (required)"

  SectionIn RO

  DetailPrint ${VERSION}

  ; Set output path to the installation directory.
  SetOutPath $INSTDIR\bin

  File /r ${BUILD_PRODUCTS_BASE}\*

  SetOutPath $INSTDIR

  ; Write the installation path into the registry
  WriteRegStr HKLM "Software\FrankHunleth\fwup-gui" "Install_Dir" "$INSTDIR"

  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "DisplayName" "fwup-gui"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "Publisher" "Frank Hunleth"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "DisplayVersion" "${VERSION}"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui" "NoRepair" 1
  WriteUninstaller "uninstall.exe"

SectionEnd

Section "Start Menu Shortcuts"
  SetShellVarContext all

  CreateShortCut "$SMPROGRAMS\fwup-gui.lnk" "$INSTDIR\bin\fwup-gui.exe" "" "$INSTDIR\bin\fwup-gui.exe" 0
SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\fwup-gui"
  DeleteRegKey HKLM "Software\FrankHunleth\fwup-gui"

  ; Remove files
  RMDir /r $INSTDIR\bin

  ; Uninstaller
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  SetShellVarContext all
  Delete "$SMPROGRAMS\fwup-gui.lnk"

  ; Remove directories used
  RMDir "$INSTDIR"

SectionEnd


;--------------------------------
;Version Information

VIProductVersion "${VERSION}.0"
VIAddVersionKey /LANG=${LANG_ENGLISH} "ProductName" "fwup-gui"
VIAddVersionKey /LANG=${LANG_ENGLISH} "CompanyName" "Frank Hunleth"
VIAddVersionKey /LANG=${LANG_ENGLISH} "LegalCopyright" "Copyright (C) 2018"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileDescription" "Setup Application"
VIAddVersionKey /LANG=${LANG_ENGLISH} "FileVersion" "${VERSION}"
