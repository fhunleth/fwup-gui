#1/bin/bash

set -e

FWUP_VERSION=1.0.0

WORK=$PWD/work

# Build a static version of fwup first
if [[ ! -e $WORK/staging/bin/fwup ]]; then
    mkdir -p $WORK
    cd $WORK
    curl -LO https://github.com/fhunleth/fwup/releases/download/v$FWUP_VERSION/fwup-$FWUP_VERSION.tar.gz
    tar xf ../fwup-$FWUP_VERSION.tar.gz
    cd fwup-$WORK
    ./scripts/download_deps.sh
    ./scripts/build_deps.sh
    PKG_CONFIG_PATH=$WORK/fwup-$FWUP_VERSION/build/host/deps/usr/lib/pkgconfig ./configure --enable-shared=no --prefix=$WORK/staging
    make -j4
    make install
fi


