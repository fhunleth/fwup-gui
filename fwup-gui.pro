#-------------------------------------------------
#
# Project created by QtCreator 2016-06-13T16:39:11
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fwup-gui
TEMPLATE = app

SOURCES += src/main.cpp\
    src/SelectFilePage.cpp \
    src/FwupWizard.cpp \
    src/ConfirmFilePage.cpp \
    src/SelectDestinationPage.cpp \
    src/BurningPage.cpp \
    src/ManageKeysDialog.cpp \
    src/AddKeyDialog.cpp \
    src/NamedKey.cpp \
    src/NamedKeyTableModel.cpp \
    src/Fwup.cpp

HEADERS  += \
    src/SelectFilePage.h \
    src/FwupWizard.h \
    src/ConfirmFilePage.h \
    src/SelectDestinationPage.h \
    src/BurningPage.h \
    src/ManageKeysDialog.h \
    src/AddKeyDialog.h \
    src/NamedKey.h \
    src/NamedKeyTableModel.h \
    src/Fwup.h

FORMS    += \
    src/SelectFilePage.ui \
    src/ConfirmFilePage.ui \
    src/SelectDestinationPage.ui \
    src/BurningPage.ui \
    src/ManageKeysDialog.ui \
    src/AddKeyDialog.ui

RESOURCES += \
    ui.qrc

DISTFILES += \
    ui/fwup-pup256x256.png \
    TODO.md \
    fwup-gui.rc \
    fwup-gui.exe.manifest

unix {
    target.path = /usr/bin
    INSTALLS += target
}

win32 {
    # This should work for MSVC
    # QMAKE_LFLAGS += /MANIFESTUAC:"level='requireAdministrator' uiAccess='false'"

    # This is for mingw
    RC_ICONS = fwup-pup64x64.ico
    RC_FILE = fwup-gui.rc
}


